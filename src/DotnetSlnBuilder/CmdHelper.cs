﻿using System.Diagnostics;

namespace Builder;
/// <summary>
/// CMD命令帮助类
/// </summary>
public class CmdHelper
{
    private static string CmdPath = @"C:\Windows\System32\cmd.exe";

    /// <summary>
    /// 执行CMD命令
    /// </summary>
    /// <param name="cmd">要执行的命令</param>
    /// <param name="output"></param>
    public static void RunCmd(string cmd)
    {
        using (Process p = new Process())
        {
            p.StartInfo.FileName = CmdPath;
            p.StartInfo.UseShellExecute = false; //是否使用操作系统shell启动
            p.StartInfo.RedirectStandardInput = true; //接受来自调用程序的输入信息
            p.StartInfo.RedirectStandardOutput = true; //由调用程序获取输出信息
            p.StartInfo.RedirectStandardError = true; //重定向标准错误输出
            p.StartInfo.CreateNoWindow = true; //不显示程序窗口
            p.Start(); //启动程序

            //向cmd窗口写入命令
            p.StandardInput.WriteLine(cmd);
            p.StandardInput.AutoFlush = true;

            //获取执行信息
            var output = p.StandardOutput.ReadToEnd();
            Console.WriteLine(output);

            p.WaitForExit(); //等待程序执行完退出进程
            p.Close();
        }
    }
}